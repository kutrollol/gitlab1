<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Lấy thông tin từ web-API</title>
	<link href="">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
	<?php
	$url='https://newsapi.org/v2/everything?q=tesla&from=2022-08-30&sortBy=publishedAt&apiKey=9f709997d5054c9f8178eb2f565e452e';
	$reponse = file_get_contents($url);
	$newdata = json_decode($reponse);
	?>
	<div class="container">
		<center><h1>Lấy tin tức từ web theo API</h1></center>
	</div>
	<div class="container-left">
		<?php
		    foreach ($newdata->articles as $News) {
		     	# code...
		     } {
		?>
		    <div class="row NewGrid">
		    	<div class="col-md-3">
		    		<img src="<?php echo $News->urlToImage?>" alt="News thumbnail">	
		    	</div>
		    	<div calss="col-md-9">
		    		<h2>Tiêu đề:<?php echo $News->title ?></h2>
		    		<h5>Mô tả:<?php echo $News->description ?></h5>
		    		<p>Nội dung:<?php echo $News->content ?></p>
		    		<h5>Tác giả:<?php echo $News->author ?></h5>
		    		<h5>Thời gian cập nhật:<?php echo $News->publishedAt ?></h5>
		    	</div>
		    </div>
		<?php
		    }
		?>
	</div>
</body>
</html>
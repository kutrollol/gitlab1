import xml.etree.cElementTree as ET

tree = ET.ElementTree(file='book.xml')
root = tree.getroot()

for books in root:
    if(books.tag == 'book'):
       print(books.get('id' + '\n'))
       for attr in books:
           if(attr.tag == 'author'
              or attr.tag == 'title'
              or attr.tag == 'genre'
              or attr.tag == 'price'
              or attr.tag == 'publish_date'
              or attr.tag == 'description'):
                   print(attr.text)
